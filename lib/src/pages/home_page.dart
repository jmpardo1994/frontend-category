import 'package:categoria/utils/httpClient.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';


class HomePage extends StatefulWidget {

  @override
    _HomePage createState()=> _HomePage();

}

class _HomePage extends State<HomePage>{

  List data;
  List categoriesData;
  getCategories() async {
    HttpClient client = new HttpClient();
    http.Response response = await client.get('http://10.0.2.2:4000/categorys');
    print(response.body);
    data = json.decode(response.body);
    setState(() {
      categoriesData = data;
    });
  }

  @override 
  void initState(){
    super.initState();

    getCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Categoria'),
        backgroundColor: Colors.indigoAccent,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: (){},
          )
        ],

      ),
      body: ListView.builder(
        padding: EdgeInsets.all(20.0),

          itemCount: categoriesData == null ? 0 : categoriesData.length,
          itemBuilder: (BuildContext context, int index) => _cardCategory(context, index),
      )

    );
  }
   Widget _cardCategory( BuildContext context, int index){

     return Card(

        child: Column(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.category, color:Colors.blue),
              title: Text('${categoriesData[index]["nomcategory"]}'),
            ),
            Row(
                
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  FlatButton(
                    child: Text('Ver'),
                    onPressed: () => Navigator.pushNamed(context, 'ejemplo'),
                  )
                ],
            ),
          ],
        ),
       
     );

   }
}