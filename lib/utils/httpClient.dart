import 'package:http/http.dart' as http;

class HttpClient {
  Future<http.Response> get(uri) {
    return http.get(uri);
  }
}
