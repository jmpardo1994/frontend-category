import 'package:categoria/src/pages/ejemplo_page.dart';
import 'package:categoria/src/pages/home_page.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Categoria',
      initialRoute: '/',
      routes: {
        '/' : (BuildContext context) => HomePage(),
        'ejemplo' : (BuildContext context) => Ejemplo(),
      },
    );
  }
}